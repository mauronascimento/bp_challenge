FROM python:3.7-slim-buster as base

ARG ENV="production"
ENV PYTHONUNBUFFERED 1
ENV APPDIR /code

WORKDIR ${APPDIR}

RUN apt-get update && \
    apt-get install -y git build-essential default-libmysqlclient-dev gettext && \
    apt-get install -y cron
RUN apt-get -y -qq update && apt-get -y -qq upgrade
COPY requirements/ requirements/

RUN pip install --no-cache-dir -r requirements/$ENV.txt

COPY . ${APPDIR}/

RUN python manage.py collectstatic --noinput

EXPOSE 8000

ADD scripts/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
