# README #

Test repository for the company Brasil Prev.


### Environment setup ###

* Docker version 19.03.8 and docker-compose version 1.17.1 are required on the machine.

### Initialization ###

* To start the project, just follow the steps below:
*
1. [Clone the project](https://bitbucket.org/mauronascimento/bp_challenge/src/master/)
2. Go to the project directory and give the command "docker-compose up --build"

### Testing the application ###

* The application consists of: web app, app worker, redis and database.
* The worker will start the game and record the information in the bank every 1 minute.
* The web application responds on port 8000
* Web application details:
    - http://0.0.0.0:8000/docs/ -> api documentation
    - http://0.0.0.0:8000/all_results/ -> all pagination results
    - http://0.0.0.0:8000/match/ -> match results
    - http://0.0.0.0:8000/round/ -> round results