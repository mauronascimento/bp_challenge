import os

from decouple import config
from redis import ConnectionPool, SSLConnection

# Project specific settings
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

STATIC_URL = "/static/"
STATICFILES_DIRS = [os.path.join(BASE_DIR, "static")]
VENV_PATH = os.path.dirname(BASE_DIR)
STATIC_ROOT = os.path.join(VENV_PATH, "static_root")

ENVIRONMENT: str = config("ENVIRONMENT", default="local")

TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True


SECRET_KEY = config(
    "SECRET_KEY", default="&wold!g-s5fcy1$ub11hgvqk=$K+-5pc+kdr6y(0#h5l&z0#pw"
)

ALLOWED_HOSTS = ["127.0.0.1:*", "localhost:*", "0.0.0.0:*", "*"]


DEBUG = config("DEBUG", default=False, cast=bool) and ENVIRONMENT == "local"


# Apps

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "rest_framework",
    "huey.contrib.djhuey",
    "bp_game.apps.BpGameConfig",
]

# Middlwares
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

# url
ROOT_URLCONF = "bp_challenge.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "bp_challenge.wsgi.application"

# Database
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": config("DB_NAME", default=""),
        "USER": config("DB_USER", default=""),
        "PASSWORD": config("DB_PASSWORD", default=""),
        "HOST": config("DB_HOST", default=""),
        "PORT": config("DB_PORT", default=""),
    }
}


# DRF
REST_FRAMEWORK = {
    "DEFAULT_RENDERER_CLASSES": ("rest_framework.renderers.JSONRenderer",),
    "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.AllowAny"],
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    "DEFAULT_SCHEMA_CLASS": "rest_framework.schemas.coreapi.AutoSchema",
    "PAGE_SIZE": 10,
}


# Redis
REDIS_HOST = config("REDIS_HOST", default="")
REDIS_PORT = config("REDIS_PORT", default="")
REDIS_DB = config("REDIS_DB", default="")
REDIS_PASSWORD = config("REDIS_PASSWORD", default="")
REDIS_SSL_ACTIVE = config("REDIS_SSL_ACTIVE", default=True, cast=bool)


# Huey
REDIS_POOL_KWARGS = {
    "host": REDIS_HOST,
    "port": REDIS_PORT,
    "db": REDIS_DB,
    "password": REDIS_PASSWORD,
    "max_connections": 100,
}

if REDIS_SSL_ACTIVE:
    REDIS_POOL_KWARGS["connection_class"] = SSLConnection

connection_pool = ConnectionPool(**REDIS_POOL_KWARGS)

HUEY = {
    "huey_class": "huey.RedisHuey",
    "connection": {"connection_pool": connection_pool},
    "consumer": {
        "workers": 1,
        "worker_type": "thread",
        "check_worker_health": True,
        "health_check_interval": 1,
    },
    "immediate": False,
}
