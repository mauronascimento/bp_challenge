# from django.http import HttpResponse
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.views import serve
from django.urls import path
from django.views.decorators.cache import never_cache
from rest_framework import routers
from rest_framework.documentation import include_docs_urls

from bp_game.views import GetAllResultsView, MatchDetailView, RoundDetailView

# schema_view = get_swagger_view(title="Brasil Prev Challenge")


router = routers.SimpleRouter()
urlpatterns = router.urls

urlpatterns += [
    path("docs/", include_docs_urls(title="Brasil Prev Challenge")),
    url(r"^all_results/?$", GetAllResultsView.as_view(), name="Resultados"),
    url(r"^match/(?P<pk>[0-9]+)/?$", MatchDetailView.as_view(), name="MatchDetailView"),
    url(r"^round/(?P<pk>[0-9]+)/?$", RoundDetailView.as_view(), name="MatchDetailView"),
    path("admin/", admin.site.urls),
] + static(settings.__getattr__("STATIC_URL"), view=never_cache(serve))
