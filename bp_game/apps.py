from django.apps import AppConfig


class BpGameConfig(AppConfig):
    name = "bp_game"
