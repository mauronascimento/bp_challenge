import random

from .jogador import Jogador


class Aleatorio(Jogador):
    def __init__(self, nome):
        return super().__init__(nome)

    def deve_comprar(self, propriedade):
        return self.tem_saldo_positivo() and self.tem_probabilidade()

    def tem_probabilidade(self):
        return random.randint(0, 2) > 1
