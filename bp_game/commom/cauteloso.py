from .jogador import Jogador


class Cauteloso(Jogador):
    def __init__(self, nome):
        return super().__init__(nome)

    def deve_comprar(self, propriedade):
        return (
            self.tem_saldo_positivo()
            and (self.saldo - propriedade.valor_da_venda) >= 80
        )
