from .jogador import Jogador


class Exigente(Jogador):
    def __init__(self, nome):
        return super().__init__(nome)

    def deve_comprar(self, propriedade):
        return self.tem_saldo_positivo() and propriedade.valor_do_aluguel >= 50
