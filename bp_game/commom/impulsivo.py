from .jogador import Jogador


class Impulsivo(Jogador):
    def __init__(self, nome):
        super().__init__(nome)

    def deve_comprar(self, propriedade):
        return self.tem_saldo_positivo()
