class Jogador:
    def __init__(self, nome):
        self.nome = nome
        self.saldo = 300
        self.posicao = 0

    def tem_saldo_positivo(self):
        return self.saldo > 0

    def pular_posicao(self, posicao, quantidade_de_propriedade):
        for i in range(posicao):
            self.posicao += 1
            self.posicao = (
                0 if self.posicao == quantidade_de_propriedade else self.posicao
            )

    def esta_falido(self):
        return not self.tem_saldo_positivo()
