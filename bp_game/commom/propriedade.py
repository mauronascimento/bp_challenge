class Propriedade:
    def __init__(self, nome, valor_da_venda, valor_do_aluguel):
        self.nome = nome
        self.valor_da_venda = valor_da_venda
        self.valor_do_aluguel = valor_do_aluguel
        self.proprietario = None

    def esta_vendida(self):
        return self.proprietario is not None

    def pertence(self, jogador):
        return self.esta_vendida() and self.proprietario.nome == jogador.nome
