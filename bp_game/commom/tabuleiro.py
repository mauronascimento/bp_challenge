import random

from ..models import Resultados
from .aleatorio import Aleatorio
from .cauteloso import Cauteloso
from .exigente import Exigente
from .impulsivo import Impulsivo
from .propriedade import Propriedade


class Tabuleiro:
    def __init__(self):
        self.vencedor = None
        # necessita de 20 propri
        self.propriedades = [
            Propriedade("Alphaville", 300, 100),
            Propriedade("Jandira", 200, 50),
        ]
        self.jogadores = random.choices(
            [
                Impulsivo("Impulsivo"),
                Aleatorio("Aleatorio"),
                Exigente("Exigente"),
                Cauteloso("Cauteloso"),
            ],
            k=4,
        )

    def iniciar(self, partida):

        retorno = []

        for rodada in range(0, 1000):
            for vez_do_jogador in filter(
                lambda jogador: not jogador.esta_falido(), self.jogadores
            ):
                posicao = self.__rodar_dado()
                vez_do_jogador.pular_posicao(posicao, len(self.propriedades))
                propriedade = self.propriedades[vez_do_jogador.posicao]
                if self.tem_que_pagar_aluguel(propriedade, vez_do_jogador):
                    self.pagar_aluguel(propriedade, vez_do_jogador)
                elif vez_do_jogador.deve_comprar(propriedade):
                    self.comprar_propriedade(propriedade, vez_do_jogador)
                if vez_do_jogador.esta_falido():
                    self.remover_as_propriedades(vez_do_jogador)

                retorno.append(
                    {
                        "nome": self.obter_jogador_com_maior_saldo().nome,
                        "saldo": self.obter_jogador_com_maior_saldo().saldo,
                        "posicao": self.obter_jogador_com_maior_saldo().posicao,
                    }
                )
        self.__calcula(retorno, rodada, partida)

    def obter_jogador_com_maior_saldo(self):
        return max(self.jogadores, key=lambda jogador: jogador.saldo)

    def remover_as_propriedades(self, vez_do_jogador):
        propriedades_do_jogador = filter(
            lambda propriedade: propriedade.pertence(vez_do_jogador), self.propriedades
        )
        for propriedade in propriedades_do_jogador:
            propriedade.proprietario = None

    def tem_que_pagar_aluguel(self, propriedade, vez_do_jogador):
        return propriedade.esta_vendida() and propriedade.proprietario != vez_do_jogador

    def comprar_propriedade(self, propriedade, vez_do_jogador):
        propriedade.proprietario = vez_do_jogador
        vez_do_jogador.saldo -= propriedade.valor_da_venda

    def __rodar_dado(self):
        return random.randint(1, 6)

    def pagar_aluguel(self, propriedade, vez_do_jogador):
        vez_do_jogador.saldo -= propriedade.valor_do_aluguel
        proprietario = self.obter_jogador(propriedade.proprietario.nome)
        proprietario.saldo += propriedade.valor_do_aluguel

    def obter_jogador(self, nome):
        return next(jogador for jogador in self.jogadores if jogador.nome == nome)

    def __calcula(self, retorno, rodada, partida):
        ganhadores = []
        comportamento_mais_vence = ""

        for resultados in retorno:
            ganhadores.append(resultados["nome"])

        impulsivo = ganhadores.count("Impulsivo")
        aleatorio = ganhadores.count("Aleatorio")
        exigente = ganhadores.count("Exigente")
        cauteloso = ganhadores.count("Cauteloso")

        if impulsivo > aleatorio and impulsivo > exigente and impulsivo > cauteloso:
            comportamento_mais_vence = "Impulsivo"
        elif aleatorio > impulsivo and aleatorio > exigente and aleatorio > cauteloso:
            comportamento_mais_vence = "Aleatorio"
        elif exigente > impulsivo and exigente > aleatorio and exigente > cauteloso:
            comportamento_mais_vence = "Exigente"
        else:
            comportamento_mais_vence = "Cauteloso"
        import json

        result = {
            "quantidade": {
                "percentual": {
                    "impulsivo": "{0}%".format((impulsivo / len(retorno)) * 100),
                    "aleatorio": "{0}%".format((aleatorio / len(retorno)) * 100),
                    "exigente": "{0}%".format((exigente / len(retorno)) * 100),
                    "cauteloso": "{0}%".format((cauteloso / len(retorno)) * 100),
                },
                "comportamento_mais_vence": comportamento_mais_vence,
            }
        }
        Resultados.objects.create(
            partida=partida, rodada=rodada, resultado=json.dumps(result)
        )


if __name__ == "__main__":
    Tabuleiro().iniciar()
