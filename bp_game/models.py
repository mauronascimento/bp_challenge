from django.db import models
from datetime import datetime


class Resultados(models.Model):
    id = models.AutoField(primary_key=True, db_index=True)
    partida = models.IntegerField(editable=True, null=True, blank=True)
    rodada = models.IntegerField(editable=True, null=True, blank=True)
    resultado = models.TextField("resultado", blank=True, null=True)
    created_at = models.DateTimeField(
        "Created in",
        default=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        editable=False,
        null=False,
    )
    updated_at = models.DateTimeField(
        "Updated in", auto_now_add=False, default=None, editable=True, null=True
    )
    deleted_at = models.DateTimeField(
        "Deleted in", auto_now_add=False, default=None, editable=True, null=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Resultados"
        verbose_name_plural = "Resultados"
        ordering = ["created_at"]
