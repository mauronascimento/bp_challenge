from huey import crontab
from huey.contrib.djhuey import db_periodic_task, lock_task

from bp_game.commom.tabuleiro import Tabuleiro


@db_periodic_task(crontab(minute="*"))
@lock_task("open_alerts")
def start():
    for partida in range(0, 300):
        Tabuleiro().iniciar(partida)
