# from django.conf import settings
# import requests
from rest_framework.views import APIView
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework import status
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.db import DatabaseError
from rest_framework.generics import GenericAPIView
from rest_framework.pagination import LimitOffsetPagination
from .serializers import ResultadosSerializer
from .models import Resultados


class GetAllResultsView(GenericAPIView):

    serializer_class = ResultadosSerializer
    pagination = LimitOffsetPagination

    @method_decorator(cache_page(60))
    def get(self, request):
        try:
            self.pagination.default_limit = 20
            all_result = Resultados.objects.all()
            page = self.paginate_queryset(all_result)

            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

            serializer = self.get_serializer(all_result, many=True)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        except DatabaseError as err:
            return Response(
                data={"message": f"{str(err)}"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        except APIException as err:
            return Response(
                data={"message": f"{str(err)}"}, status=APIException.status_code
            )
        except Exception:
            return Response(
                data={"message": "generic error."}, status=status.HTTP_400_BAD_REQUEST
            )


class MatchDetailView(APIView):

    serializer_class = ResultadosSerializer

    def get(self, request, pk):
        try:
            match = Resultados.objects.filter(partida=pk)
            serializer = self.serializer_class(match, many=True)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        except DatabaseError as err:
            return Response(
                data={"message": f"{str(err)}"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        except APIException as err:
            return Response(
                data={"message": f"{str(err)}"}, status=APIException.status_code
            )
        except Resultados.DoesNotExist:
            return Response(
                data={"message": "partida nonexistent"},
                status=status.HTTP_404_NOT_FOUND,
            )

        except Exception as err:
            return Response(
                data={"message": str(err)}, status=status.HTTP_400_BAD_REQUEST
            )


class RoundDetailView(APIView):

    serializer_class = ResultadosSerializer

    def get(self, request, pk):
        try:
            round = Resultados.objects.filter(rodada=pk)
            serializer = self.serializer_class(round, many=True)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        except DatabaseError as err:
            return Response(
                data={"message": f"{str(err)}"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        except APIException as err:
            return Response(
                data={"message": f"{str(err)}"}, status=APIException.status_code
            )
        except Resultados.DoesNotExist:
            return Response(
                data={"message": "partida nonexistent"},
                status=status.HTTP_404_NOT_FOUND,
            )

        except Exception as err:
            return Response(
                data={"message": str(err)}, status=status.HTTP_400_BAD_REQUEST
            )
