#!/bin/bash

## Usage:
## [options] ./scripts/build_image.sh [env]
##
## Options:
##   TAG: desired tag for image built
##     example:
##     TAG=mibi:v0.1.0 ./scripts/build_image.sh
##
##   SSH_KEY: path to ssh key with read access to private dependencies
##     example:
##     SSH_KEY=~/.ssh/mibi ./scripts/build_image.sh
##
##  Arguments:
##  env: Build image using an environment requirements file
##      example:
##      ./scripts/build_image.sh local

usage() {
    sed -E -ne 's/^##(.*)/\1/p' $0
    exit -1
}

[[ $1 != "" ]] && [[ $1 != "local" ]] && usage

# This may be needed in some cases
docker pull docker.io/docker/dockerfile:experimental

# DOCKER_BUILDKIT is required to use experimental syntax
# Refer to: https://docs.docker.com/develop/develop-images/build_enhancements/
export DOCKER_BUILDKIT=1

BUILD_ARGS=""
# If we are build an image using local requirements
# we need to specify the local Dockerfile
if [[ $1 == "local" ]]; then
    BUILD_ARGS="--build-arg ENV=$1 $BUILD_ARGS"
fi

# default tag should depend on docker-compose service name
docker build -t ${TAG:-mibi_app} $BUILD_ARGS . --no-cache
