#!/usr/bin/env bash

docker-compose run --rm \
    --entrypoint "/bin/bash -c" \
    app "mypy ."
