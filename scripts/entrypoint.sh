#!/bin/sh
set -e


if [ $# -eq 0 ]; then
  if [ "$TYPE_APPLICATION" = 'worker' ]; then
    cd /code
    exec python manage.py run_huey -V
  fi
else
  exec "$@"
fi