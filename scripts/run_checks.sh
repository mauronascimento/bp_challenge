#!/usr/bin/env bash

docker-compose run --rm \
    --entrypoint "/bin/bash -c" \
    app "coverage run manage.py test -v2 && coverage report && mypy ."
